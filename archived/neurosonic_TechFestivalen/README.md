# Neurosonic TechFestivalen versions
This folder contains the designs made by Michail Nenkov and Bertilla Frabris, 
for the MindLAB Campfire Experience setup 
in Copenhagen at TechFestivalen's Makers exhibition in August 2019.
 


## CampfireNewDesign_adhoc.con
Made for children.
## CampfireNewDesignLinegrid.con
Made for adults.