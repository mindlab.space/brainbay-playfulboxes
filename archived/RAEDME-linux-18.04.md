![MindLAB logo](https://i.ibb.co/ZJJY7S7/Original-200x200.png "MindLAB logo") 
# Neurosonic for linux! (OpenBCI Ganglion, BrainBay, multimedia and Ubuntu 18.04)
This folder contains BrainBay designs (configurations) implemented on linux.   
As the system used to develop these design is Ubuntu 18.04, the instructions to setup the environemnt 
might be quite distro specific.  

How to run OpenBci Ganglion, BrainBay, multimedia, (including midi) on linux?  

Follow the instruction below to setup that include: 
* OpenBCI Ganglion
* BrainBay
* MIDI Boxes 
* LINUX (Ubuntu 18.04)

## Getting Started

### User permissions
The user must have ```dialout``` permission:  
```
sudo usermod -a -G dialout the-user-name
```


### Software Prerequisites
- Wine ^4
- Qsynth (for the midi box)

### Installing
Install BrainBay using Wine:
`$ wine Setup_BrainBay.exe`   
BrainBay will be installed in `/home/*username*/BraynBay` folder.   


### Set up for Linux
- OpenBCI_HUB:   
    It is important the hub is runnung before BrainBay is started or it won't be 
    possible to connect the OpenBCI Ganglion.
    - Download the Linux version: it will run as local service and it will avalilable to BrainBay running as Wine application.   
    The HUB is part of the OpenBCI_GUI application. Find it at https://openbci.com/index.php/downloads   
    - Once the GUI has been downloaded loacally, open a terminal from its folder and start the HUB as root:   
    `$ sudo ./data/OpenBCIHub/OpenBCIHub`   
    The terminal should log:
    `app is ready`   
    The terminal will keep log Hub's messages for the rest of the session.  

- Qsynth
In order for BrainBay to see a MIDI device other than the midi-mapper (Wine), Qsynth should be started before BrainBay.


- BrainBay    
    - Start BraynBay using wine:   
    `$ wine /home/*username*/BraynBay/brainBay.exe`
    - Connect the OpenBCI Ganglion to the application via BLED112 (bluetooth) by right click on the
    Ganglion box to access the Ganglion connection control panel.
    - MIDI:
    	- Enable the new MIDI channel for the MIDI boxes: from the menu _Options_ -> _Application Settings_ -> _MIDI available devices_.   
    The midi boxes will list the Qsynth's midi channels in the drop-down menu of their control panel 
    (right click on a midi box)

# Enjoy  BrainBay on linux!
![brainbay](./brainbay.jpg "brainbay") 
