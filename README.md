![MindLab logo](https://i.ibb.co/ZJJY7S7/Original-200x200.png "MindLab logo") 

# MindLab's BrainBay Configurations
This repository hosts [brainbay](http://www.shifz.org/brainbay/ "Brainbay's Homepage")'s configuirations, developed at [MindLab.space](http://mindlab.space "MindLab's Homepage").   


## Getting Started
1. Install [BrainBay](https://github.com/ChrisVeigl/BrainBay "BrainBay GitHub repository").
2. Open a design (.con files) from this repository.
3. Play


## Contributing
Anyone can contribute to this repository with his/her own ideas.      
How to do it? Quite simple, but you need to know some GIT:   

* (Git) clone this repository
* (Git) checkout develop branch
* (Git) add your design root
* (Git) commit your new design and don't forget to attach a nice message.
* (Git) push your contribution to the online branch origin/develop 
* (GitLab) Open a merge request to main branch.
Thanks! :)


### Code of Conduct
#### Our Pledge
In the interest of fostering an open and welcoming environment, we as contributors and maintainers pledge to making participation in our project and our community a harassment-free experience for everyone, regardless of age, body size, disability, ethnicity, gender identity and expression, level of experience, nationality, personal appearance, race, religion, or sexual identity and orientation.


#### Our Standards
Examples of behavior that contributes to creating a positive environment include:  

- Using welcoming and inclusive language
- Being respectful of differing viewpoints and experiences
- Gracefully accepting constructive criticism
- Focusing on what is best for the community
- Showing empathy towards other community members

Examples of unacceptable behavior by participants include:   

- The use of sexualized language or imagery and unwelcome sexual attention or advances
- Trolling, insulting/derogatory comments, and personal or political attacks
- Public or private harassment
- Publishing others' private information, such as a physical or electronic address, without explicit permission
- Other conduct which could reasonably be considered inappropriate in a professional setting


## Authors
- The [MindLab.space](http://mindlab.space "MindLab's Homepage") team.   
Read about the contributors of specific designs into the related documentation files.


## License
All the design are licensed under the 
[BSD-3](https://opensource.org/licenses/BSD-3-Clause) license.   


